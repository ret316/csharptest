using Microsoft.Extensions.Logging;
using NSubstitute;
using Pr1.Api.Controllers;

namespace Pr1.Tests;

public class ApiTests
{
    [Fact]
    public void Test1()
    {
        var logger = Substitute.For<ILogger<WeatherForecastController>>();
        var controller = new WeatherForecastController(logger);

        var result = controller.Get();

        Assert.NotNull(result);
    }
}